%global _description \
Pygments is a generic syntax highlighter suitable for use \
in code hosting, forums, wikis or other applications that \
need to prettify source code. Highlights are: \
	* a wide range of over 500 languages and other text \
	  formats is supported \
	* special attention is paid to details that increase \
	  highlighting quality \
	* support for new languages and formats are added easily; \
	  most languages use a simple regex-based lexing mechanism \
	* a number of output formats is available, among them HTML, \
	  RTF, LaTeX and ANSI sequences \
	* it is usable as a command-line tool and as a library \
	* … and it highlights even Perl 6!
%bcond_with docs
Name:           python-pygments
Summary:        Syntax highlighting engine written in Python
Version:        2.18.0
Release:        1
License:        BSD-2-Clause
URL:            http://pygments.org/
Source0:        https://pypi.org/packages/source/P/Pygments/pygments-%{version}.tar.gz

BuildArch:      noarch

%description
%{_description}

%package -n python3-pygments
BuildRequires:  python3-devel, python3-setuptools
BuildRequires:  python3-setuptools_scm
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-hatchling

%if %{with docs}
BuildRequires:  python3-sphinx python3-pip
%endif

Summary:        Syntax highlighting engine written in Python
%{?python_provide:%python_provide python3-pygments}

%description -n python3-pygments
%{_description}

%package_help

%prep
%autosetup -p1 -n pygments-%{version}

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-pygments
%license LICENSE
%{python3_sitelib}/*
%{_bindir}/pygmentize

%files help
%if %{with docs}
%lang(en) %{_mandir}/man1/pygmentize.1*
%doc build/sphinx/html
%endif

%changelog
* Wed May 22 2024 lilu <lilu@kylinos.cn> - 2.18.0-1
- Update package to version 2.18.0
- Make background colors in the image formatter work with Pillow 10.0
- Replace Pyflakes linter with Ruff

* Tue Jan 30 2024 zhaotianyang_z <zhaotianyang4@huawei.com> - 2.17.2-1
- upgrade package to version 2.17.2

* Tue Nov 7 2023 Dongxing Wang <dxwangk@isoftstone.com> - 2.16.1-1
- Update package to version 2.16.1

* Sun Jun 25 2023 li-miaomiao_zhr <mmlidc@isoftstone.com> -  2.15.1-1
- update package to the latest version of 2.15.1

* Tue Jan 17 2023 caofei <caofei@xfusion.com> -  2.10.0-5
- Fix "do concurrent" and "go to" keywords in the Fortran

* Fri Jan 13 2023 caofei <caofei@xfusion.com> -  2.10.0-4
- fixed typo

* Wed May 11 2022 wulei <wulei80@h-partners.com> - 2.10.0-3
- License compliance rectification

* Thu Feb 03 2022 Liu Zixian <liuzixian4@huawei.com> -2.10.0-2
- Disable doc temporarily to break build dependency.

* Mon Dec 20 2021 renhongxun <renhongxun@huawei.com> - 2.10.0-1
- Upgrade version to 2.10.0

* Fri Aug 06 2021 OpenStack_SIG <openstack@openeuler.org> - 2.8.1-1
- Update version to 2.8.1

* Wed Jan 13 2021 SimpleUpdate Robot <tc@openeuler.org> - 2.7.4-1
- Upgrade to version 2.7.4

* Tue Jul 28 2020 jinzhimin<jinzhimin2@huawei.com> - 2.6.1-1
- update to 2.6.1-1

* Fri Dec 6 2019 caomeng<caomeng5@huawei.com> - 2.2.0-15
- Package init
